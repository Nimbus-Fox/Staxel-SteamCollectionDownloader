﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using AngleSharp;

namespace SteamCollectionParser {
    class Program {
        static void Main(string[] args) {
            var location = $"https://steamcommunity.com/sharedfiles/filedetails/?id={args[0]}";

            var config = Configuration.Default.WithDefaultLoader();

            var document = BrowsingContext.New(config).OpenAsync(location).Result;

            var cells = document.QuerySelectorAll(".collectionItem");
            var sb = new StringBuilder();

            var ids = new List<string>();

            foreach (var cell in cells) {
                var link = cell.QuerySelector(".collectionItemDetails").Children[0].Attributes.GetNamedItem("href")
                    .Value;

                ids.Add(link.Substring(link.LastIndexOf('=') + 1));

                sb.Append($"+workshop_download_item {args[1]} {link.Substring(link.LastIndexOf('=') + 1)} ");
            }

            var startInfo = new ProcessStartInfo(args[2], string.Format(args[3], sb));

            var process = new Process { StartInfo = startInfo };

            process.Start();

            process.WaitForExit();

            Console.WriteLine($"Installing: {string.Join(", ", ids)}");

            foreach (var id in ids) {
                var file = new DirectoryInfo(Path.Combine(args[4], id)).GetFiles()[0];

                var modStartInfo = new ProcessStartInfo(args[5], $"--buildOnInstall=false --installMod \"{Path.Combine(args[4], id, file.Name)}\"");

                var modProcess = new Process { StartInfo = modStartInfo };

                modProcess.Start();
                modProcess.WaitForExit();
            }

            SHEmptyRecycleBin(IntPtr.Zero,
                null,
                RecycleBinFlags.NOCONFIRMATION | RecycleBinFlags.NOPROGRESSUI | RecycleBinFlags.NOSOUND);
        }

        [DllImport("Shell32.dll", CharSet = CharSet.Unicode)]
        static extern uint SHEmptyRecycleBin(IntPtr hwnd, string rootPath, RecycleBinFlags flags);

        [Flags]
        enum RecycleBinFlags : uint {
            NOCONFIRMATION = 0x00000001,
            NOPROGRESSUI = 0x00000002,
            NOSOUND = 0x00000004
        }
    }
}
