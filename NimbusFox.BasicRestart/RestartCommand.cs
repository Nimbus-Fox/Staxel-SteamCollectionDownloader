﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.BasicRestart {
    public class RestartCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            Process.Start("taskkill", "/PID " + Process.GetCurrentProcess().Id);

            return "";
        }

        public string Kind => "restart";
        public string Usage => "restart - stops the server for your restart scripts to boot the server backup";
        public bool Public => false;
    }
}
